import random
import requests

class HraZvolHru(object):
    
    class ZvolHru(object):
        
        def gameOver(request):
            if request.session.get('zivoty') == 0 or request.session.get('zivoty') < 0:
                return("Game over. Push New Game.")
            else: pass
                
        
        def prazdnaOdpoved(request):
            return("Write what do you want to do: 'go' - 'play'")
        
        def losPokojeZacatek(request):
            
            #request.session['zivoty'] = 3
            #request.session.setdefault('zivoty',3)
            request.session['zivoty'] = 1
            
            pokoje = [
                    'livingroom',
                    'bedroom',
                    'bathroom'
                    ]
            losPokojZac = random.choice(pokoje)
            request.session['pokoj'] = losPokojZac
            
        def jit(request):
            
            pokoje = [
                    'livingroom',
                    'bedroom',
                    'bathroom'
                    ]
            
            pokoje.remove(request.session['pokoj'])
            
            losPokojZac = random.choice(pokoje)
            request.session['pokoj'] = losPokojZac
            
    
        def vyberPokoje(request):
            #request.session['zivoty'] = 3
            soucasnyPokoj = request.session.get('pokoj')
            zivoty = request.session.get('zivoty')
            return ("You are now in ", soucasnyPokoj,"Do you want to 'go' or 'play'? Your lifes: ", zivoty)
        
        #_,x,_, = vyberPokoje()
        
        def vyberHratJit(request):
            _,soucasnyPokoj,_,_, = HraZvolHru.ZvolHru.vyberPokoje(request)
            return ('You are now in ',soucasnyPokoj,"Do you want to 'go' or 'play'? Your lifes: ")
        
    class Hry(object):
        
        def vyberHru(request):
            
            obyvakhry = [
            'guess higher number',
            'even or odd'
            ]
        
            loznicehry = [
                'hide and seek'
                ]
            
            koupelnahry = [
                'bath'
                ]
        
            soucasnyPokoj = request.session.get('pokoj')
            
            if soucasnyPokoj == 'livingroom':
                soucasneHry = [hra for hra in obyvakhry]          
                return("In the ", soucasnyPokoj," you can play ",  soucasneHry)
            
            if soucasnyPokoj == 'bedroom':
                soucasneHry = [hra for hra in loznicehry]
                return("In the ", soucasnyPokoj," you can play ",  soucasneHry)
            
            if soucasnyPokoj == 'bathroom':
                soucasneHry = [hra for hra in koupelnahry]
                return("In the ", soucasnyPokoj," you can play ",  soucasneHry)
            
        class HraVyssiBere(object):
            
            def uvod(request):
                return('Write higher number than computer and get 1 life.')
            
            def hraVyssiBere(request, response):
                PCNumber = random.randint(0,10)
                if int(response) > PCNumber:
                    request.session['zivoty'] += 0.5
                    zivoty = request.session.get('zivoty')
                    return("You won! Computer number = ",PCNumber,"Your lifes: " ,zivoty, "Play again, write number.")
                elif int(response) <= PCNumber:
                    request.session['zivoty'] -= 0.5
                    zivoty = request.session.get('zivoty')
                    return("You lost! Computer number = ", PCNumber,"Your lifes: ",zivoty, "Play again, write number.")
                
        class HraSudaLicha(object):
            
            def uvod(request):
                return("Write 'even' or 'odd'.")
            
            def hraSudaLicha(request, response):
                PCNumber = random.randint(1,10)
                
                if PCNumber % 2 == 0 and response == 'even':
                    request.session['zivoty'] += 0.5
                    zivoty = request.session.get('zivoty')
                    return('Even! You won! Your lifes: ', zivoty)
                elif PCNumber % 2 == 1 and response == "odd":
                    request.session['zivoty'] += 0.5
                    zivoty = request.session.get('zivoty')
                    return('Odd! You won! Your lifes: ', zivoty)
                else:
                    request.session['zivoty'] -= 0.5
                    zivoty = request.session.get('zivoty')
                    return('You lost! Your lifes: ', zivoty)
                
        class HraSchovavana(object):
            
            def uvod(request):
                return ('In the bedroom are: bed, wardrobe, screen. Whwre is Hugo hidden?')
                
            def hraSchovavana(request, response):
                predmetyLoznice = [
                    'bed',
                    'wardrobe',
                    'screen'
                    ]
                PCvyber = random.choice(predmetyLoznice)
                
                if response == PCvyber:
                    request.session['zivoty'] += 0.5
                    zivoty = request.session.get('zivoty')
                    return('You won. Hugo is behind the ',PCvyber ,'. Your lifes: ',zivoty)
                else:
                    request.session['zivoty'] -= 0.5
                    zivoty = request.session.get('zivoty')
                    return('You lost. Hugo is behind the ',PCvyber,'. You can guess again. Write: bed, wardrobe or screen. Your lifes: ',zivoty)
                
        class HraKoupel(object):
            def uvod(request):
                return('Take a bath and get 1 life. Write: giant bath')
            
            def hraKoupel(request, response):
                if response == 'giant bath':
                    request.session['zivoty'] += 0.5
                    zivoty = request.session.get('zivoty')
                    return('One bath - one life. Your lifes: ',zivoty)
                    
        
    class Room(object):
        def __init__(self, nazev):
            self.nazev = nazev
            self.hry = []
            
    
        
