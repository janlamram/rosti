from random import randint


class GuessTheNumber(object):
    
    def __init__(self):
        pass
    
    global listGuessNumbers
    listGuessNumbers = []
    
    
    def addNumber(x):
        if int(x) != compGuess:
            listGuessNumbers.append(x)
        else:
            GuessTheNumber.generateNumber()
        return ", ".join(listGuessNumbers)
    
    def printNumbers():
        return ", ".join(listGuessNumbers)
    
    def generateNumber():
        global compGuess
        compGuess = randint(-100, 100)
        listGuessNumbers.clear()
        return compGuess
        
    compGuess = generateNumber()
        
    def hint(x):
        if int(x) > compGuess:
            hint = 'Random number is LOWER.'
        elif int(x) < compGuess:
            hint = 'Random number is HIGHER.'
        else: 
            hint = 'You guessed the number!'
        return hint
            
