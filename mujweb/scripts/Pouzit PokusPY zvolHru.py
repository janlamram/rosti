# -*- coding: utf-8 -*-
#from __future__ import print_function
import random
#import sys
class ZvolHru(object):
    class Room(object):
        def __init__(self, nazev):
            self.nazev = nazev
            self.hry = []
            
        def priVstupu(self, hrac):
            return("Jsi v " + self.nazev)
            
        #def povolenePrikazy(self, prikaz):
        #   return True
            
    class Hrac(object):
        def __init__(self):
            self.zivoty = 1
            self.mistnost = None
        
        def presun(self, mistnost):
            self.mistnost = mistnost
            mistnost.priVstupu(self)
        
    class Hra(object):
        def __init__(self, nazev):
            self.nazev = nazev
            
        def hraSudaLicha():
            #self.nazev = "suda-licha"
            PCNumber = random.randint(1, 10)
            odpoved = int(input('Napis: suda - licha'))
            print(PCNumber)
            if PCNumber % 2 == 0 and odpoved % 2 == 0:
                hrac.zivoty += 1
                print("""
                    Máte zivoty: {0}
                        """.format(hrac.zivoty))
            elif PCNumber % 2 == 1 and odpoved % 2 == 1:
                hrac.zivoty += 1
                print("""
                    Máte zivoty: {0}
                        """.format(hrac.zivoty))
            else:
                hrac.zivoty -= 1
                print("""
                    Máte zivoty: {0}
                        """.format(hrac.zivoty))
                
        def hraVyssiBere():
            PCNumber = random.randint(1, 10)
            odpoved = int(input('Napis: Vyšší číslo než PC.'))
            print(PCNumber)
            
            if odpoved > PCNumber:
                hrac.zivoty += 1
                print("""
                    Máte zivoty: {0}
                        """.format(hrac.zivoty))
            else:
                hrac.zivoty -= 1
                print("""
                    Máte zivoty: {0}
                        """.format(hrac.zivoty))
                
        def hraSchovavana():
            predmety = [
                'skrin',
                'postel',
                'stul'
                ]
            odpoved = input(""" V pokoji jsou tyto předměty: {0}. Kde se Jožin schovává?""".format(", ".join([predmet for predmet in predmety])))
            PCTip = random.choice(predmety)
            print(PCTip)
            
            if odpoved == PCTip:
                hrac.zivoty += 1
                print("""
                    Máte zivoty: {0}
                        """.format(hrac.zivoty))
            else:
                hrac.zivoty -= 1
                print("""
                    Máte zivoty: {0}
                        """.format(hrac.zivoty))
            
    obyvak = Room("obyvak")
    loznice = Room("loznice")

    pokoje = [
        obyvak,
        loznice
        ]

    obyvak.hry +=[
        Hra("suda-licha"),
        Hra("vyssi-bere")
        ]

    loznice.hry +=[
        Hra("schovavana"),
        ]

    hrac = Hrac()
    #hraSudaLicha = Hra('suda-licha')
    hrac.presun(random.choice(pokoje))
            
    while True:
        if hrac.zivoty == 0:
            print('Game over!')
            break
        
        if hrac.zivoty == 2:
            odpoved = input('Vyhral jsi! Hrat znovu - A')
            if odpoved == 'A':
                hrac.zivoty = 1
                print("""Jsi v {0} """.format(hrac.mistnost.nazev))
                continue
            else:
                break
        
        odpoved = input("Chceš hrát, nebo jít vedle? jit - hrat")
        
        
        #if hrac.mistnost.povolenePrikazy(odpoved) == False:
         #   print("Tento prikaz nelze nyni zadat.")
          #  continue
        
        if odpoved == "jit":
            if hrac.mistnost == loznice:
                hrac.presun(pokoje[0])
                continue
            else: 
                hrac.presun(pokoje[1])
                continue

        if odpoved =="hrat":
            print("""
                V {0} si můžeš zahrát {1}. 
                """.format(hrac.mistnost.nazev, 
                            ", nebo ".join([hra.nazev for hra in hrac.mistnost.hry])))
            
            odpoved = input("""Napis: {0}""".format(", nebo ".join([hra.nazev for hra in hrac.mistnost.hry])))
            
            if odpoved =="suda-licha":
                Hra.hraSudaLicha()
                continue
            
            if odpoved =="vyssi-bere":
                Hra.hraVyssiBere()
                continue
            
            if odpoved =="schovavana":
                Hra.hraSchovavana()
                continue
            
        
    




    
