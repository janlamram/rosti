

class TextAnalysis(object):
    
    def __init__(self):
        pass
    
    def cutTheText(text):
        poleText = ", ".join([line for line in text])
        return(poleText)
    
    def countWords(text):
        words = 0
        words = len(text.split())
        return words

    
    def countDigit(text):
        digit = 0
        for letter in text:
            if letter.isdigit():
                digit += 1
        return digit
    
    def countLetters(text):
        letters = 0
        for letter in text:
            if letter.isalpha():
                letters += 1
        return letters
    
    def countSpaces(text):
        spaces = 0
        spaces = text.count(" ")
        return spaces
            
