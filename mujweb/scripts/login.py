from django.shortcuts import render
from ..models import User

class Login(object):
    def login(request):
        if request.method == 'POST':
            inputEmail = request.POST.get('inputEmail')
            inputPassword = request.POST.get('inputPassword')
                
            if inputEmail =='' or inputPassword == '':
                return render(request, 'login.html',{
                    'login_message1': ['','Enter your e-mail and password.'],
                    })
        
            if inputEmail and inputPassword:
                exist_user_email = User.objects.filter(user_email = inputEmail).count()
                exist_user_password = User.objects.filter(user_password = inputPassword).count()
                
                if exist_user_email > 0 and exist_user_password > 0:
                    request.session['userName'] = inputEmail
                    userName = request.session.get('userName')
                        return render (request, 'index.html',{
                        'User Name': userName,
                        })
                else:
                    return render(request, 'login.html',{
                        'login_message2': ['','Incorrect e-mail address or password.'],
                        })
