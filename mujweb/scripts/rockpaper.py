from random import randint

class RockPaper(object):
    
    global pocZivoty
    pocZivoty = 0
    
    def __init__(self):
        pass
    
            
    def generateNumber(pcTip):
        pcTip = randint(1,3)
        return pcTip
            
    def rock():
        tip = 1
        global pcTip
        pcTip = 0
        pcTip = RockPaper.generateNumber(pcTip)
        global answer
        if tip == pcTip:
            answer = 'Draw.'
        elif pcTip == 2:
            answer = 'You won!'
        else: answer = 'You loose!'
        return answer    
    
    def scissors():
        tip = 2
        global pcTip
        pcTip = 0
        pcTip = RockPaper.generateNumber(pcTip)
        global answer
        if tip == pcTip:
            answer = 'Draw.'
        elif pcTip == 1:
            answer = 'You loose!'
        else: answer = 'You won!'
        return answer    
    
    def paper():
        tip = 3
        global pcTip
        pcTip=0
        pcTip = RockPaper.generateNumber(pcTip)
        global answer
        if tip == pcTip:
            answer = 'Draw.'
        elif pcTip == 1:
            answer = 'You won!'
        else: answer = 'You loose!'
        return answer  
    
    def nameTip():
        if pcTip == 1:
            pcTipName = 'Rock'
        elif pcTip == 2:
            pcTipName = 'Scissors'
        elif pcTip == 3:
            pcTipName = 'Paper'
        return pcTipName
    
    def newGame():
        global pocZivoty
        pocZivoty = 0
        return pocZivoty

    def zivoty():
        global pocZivoty
        global answer
        
        if answer == 'You loose!':
            pocZivoty -= 1
        elif answer == 'You won!':
            pocZivoty +=  1
        else:
            pocZivoty += 0
            
        return pocZivoty    
            
    
    
