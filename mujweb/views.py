from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .scripts.calculator import Calculator
from .scripts.guessthenumber import GuessTheNumber
from .scripts.textanalysis import TextAnalysis
from .scripts.rockpaper import RockPaper
#from .scripts.login import Login - nefungovalo
from .models import User
from django.contrib.auth import authenticate, login
#from django.shortcuts import render_to_response
#from django.contrib.sessions.backends.db import SessionStore
from http import cookies
from .scripts.pokus import HraZvolHru
import pdb

import logging
logger = logging.getLogger("AppPoll")
logger.info("poll - views line 7 ----")

# import pdb
# pdb.set_trace()
# from .poll import urls

# Create your views here.

def index(request):
    
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
    return render(request, 'index.html',{
                    'userName': ['User: ',kokiUser(request)],
                    })

def projects(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
    return render (request, 'projects.html',{
                    'userName': ['User: ',kokiUser(request)],
                    })

def links(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
    return render (request, 'links.html',{
                    'userName': ['User: ',kokiUser(request)],
                    })

def signup(request):
    if request.method == 'POST':
            inputEmail = request.POST.get('inputEmail')
            inputPassword = request.POST.get('inputPassword')
                
            if inputEmail =='' or inputPassword == '':
                return render(request, 'signup.html',{
                    'signup_message1': ['','Enter your e-mail and password.'],
                    })
            
            if len(inputPassword) <= 6:
                    return render(request, 'signup.html',{
                    'signup_message1': ['','Password must be longer than six letters.'],
                    })
            
            first_alpha = inputPassword[0].isalpha()
            if all(c.isalpha() == first_alpha for c in inputPassword):
                return render(request, 'signup.html',{
                    'signup_message1': ['','Password must contain at least one letter and one digit or punctuation character.'],
                    })
            
            first_lower = [x for x in inputPassword if x.isalpha()]
            second_lower = first_lower[0].islower()
            if all(d.islower() == second_lower for d in first_lower):
                return render(request, 'signup.html',{
                    'signup_message1': ['','Password must contain at least one capital and one lower-case letter.'],
                    })
            
            if inputEmail and inputPassword:
                exist_user_email = User.objects.filter(user_email = inputEmail).count()
                exist_user_password = User.objects.filter(user_password = inputPassword).count()
                
                if exist_user_email > 0:
                        return render (request, 'signup.html',{
                        'signup_message2': ['','User e-mail address already exists.'],
                        })
                else:
                    user = User()
                    user.user_email = inputEmail
                    user.user_password = inputPassword
                    user.save()
                    return render(request, 'signup.html',{
                        'signup_message3': ['','Uzivatel uuulozen.'],
                        })
    return render(request, 'signup.html')

def loginPage(request):
    if request.method == 'POST':
            inputEmail = request.POST.get('inputEmail')
            inputPassword = request.POST.get('inputPassword')
                
            if inputEmail =='' or inputPassword == '':
                return render(request, 'login_page.html',{
                    'login_message1': ['','Enter your e-mail and password.'],
                    })
        
            if inputEmail and inputPassword:
                exist_user_email = User.objects.filter(user_email = inputEmail).count()
                exist_user_password = User.objects.filter(user_password = inputPassword).count()
                
                if exist_user_email > 0 and exist_user_password > 0:
        
                    def setKoki(request):
                        request.session['userName'] = inputEmail
                        
                        if 'userName' in request.session:
                            userName = request.session.get('userName')
                            return userName
                    
                    return render (request, 'index.html', {
                            'userName': ['User: ',setKoki(request)], 
                            })
                else:
                    return render(request, 'login_page.html',{
                        'login_message2': ['','Incorrect e-mail address or password.'],
                        })
        
    return render (request, 'login_page.html')

def logout(request):
        if 'userName' in request.session:
            del request.session['userName']
        
            return render (request, 'index.html', {
                                'Logout': logout(request),
                                })
        else:
            return render (request, 'index.html')


def calculator(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    if request.method == 'POST':
        if request.POST.get('x') and request.POST.get('y'):
            x = request.POST.get('x')
            y = request.POST.get('y')
            return render(request, 'projects/calculator.html', {
                'userName': ['User: ',kokiUser(request)], 
                'calculateSum':['Sum', Calculator.soucet(x, y)],
                'calculateRozdil':['Difference', Calculator.rozdil(x, y)],
                'calculateSoucin':['Multiplication', Calculator.soucin(x, y)],
                'calculateDivision':['Division', Calculator.division(x, y)],
                })  
        else:
            return render(request,'projects/calculator.html',{
        'userName': ['User: ',kokiUser(request)],
        })
    return render(request, 'projects/calculator.html',{
        'userName': ['User: ',kokiUser(request)],
        })


def guessthenumber(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    if request.method == 'POST':
        if request.POST.get('x') and 'guess' in request.POST:
            x = request.POST.get('x')
            return render(request, 'projects/guessthenumber.html', {
                'hint':['Hint', GuessTheNumber.hint(x)],
                'addNumber':['List of guessed numbers', GuessTheNumber.addNumber(x)],
                'userName': ['User: ',kokiUser(request)],
                })  
        elif 'guess' in request.POST:
            return render(request,'projects/guessthenumber.html', {
                'hint':['Hint', 'Enter your number.'],
                'printNumbers':['List of guessed numbers', GuessTheNumber.printNumbers()],
                'userName': ['User: ',kokiUser(request)],
                })
        elif 'new_game' in request.POST:
            return render(request,'projects/guessthenumber.html', {
                'hint':['Hint', 'New number has been generated.'],
                'generateNumber': GuessTheNumber.generateNumber(),
                'userName': ['User: ',kokiUser(request)],
                })
    return render(request, 'projects/guessthenumber.html',{
        'userName': ['User: ',kokiUser(request)],
        })


def textAnalysis(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    if request.method == 'POST':
        if request.POST.get('textArea') and 'analyse' in request.POST:
            text = request.POST.get('textArea')
            return render(request, 'projects/textAnalysis.html', {
                'userName': ['User: ',kokiUser(request)],
                'textSlice':['Your text', TextAnalysis.cutTheText(text)],
                'countWords':['Sum of words', TextAnalysis.countWords(text)],
                'countDigit':['Sum of numerals', TextAnalysis.countDigit(text)],
                'countLetters':['Sum of letters', TextAnalysis.countLetters(text)],
                'countSpaces':['Sum of spaces', TextAnalysis.countSpaces(text)],
                })  
    return render(request, 'projects/textAnalysis.html',{
        'userName': ['User: ',kokiUser(request)],
        })

def rockPaper(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    if request.method == 'POST':
        if 'rock' in request.POST:
            return render(request, 'projects/rockpaper.html', {
                'userName': ['User: ',kokiUser(request)],
                'rock':['Hint', RockPaper.rock()],
                'nameTip':['Opponent tip', RockPaper.nameTip()],
                'zivoty':['Score', RockPaper.zivoty()],
                })
        elif 'scissors' in request.POST:
            return render(request, 'projects/rockpaper.html', {
                'userName': ['User: ',kokiUser(request)],
                'scissors':['Hint', RockPaper.scissors()],
                'nameTip':['Opponent tip', RockPaper.nameTip()],
                'zivoty':['Score', RockPaper.zivoty()]
                })
        elif 'paper' in request.POST:
            return render(request, 'projects/rockpaper.html', {
                'userName': ['User: ',kokiUser(request)],
                'paper':['Hint', RockPaper.paper()],
                'nameTip':['Opponent tip', RockPaper.nameTip()],
                'zivoty':['Score', RockPaper.zivoty()]
                })
        elif 'newGame' in request.POST:
            return render(request, 'projects/rockpaper.html', {
                'userName': ['User: ',kokiUser(request)],
                'newGame':RockPaper.newGame(),
                'zivoty':['Score', '0']
                })
    return render(request, 'projects/rockpaper.html',{
        'userName': ['User: ',kokiUser(request)],
        })

# pocZivoty = 'Score'

def zvolHru(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    if request.method == 'POST':
            
            if 'textyhry' in request.POST:
                response = request.POST.get('textyhry')
                
                #if response:
                
                if response == "play":
                   # HraZvolHru.Hry.hraVyssiBere(request)
                    x=HraZvolHru.Hry.vyberHru(request)
                    if x is None:
                        HraZvolHru.ZvolHru.losPokojeZacatek(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.Hry.vyberHru(request) ],
                'text3':['Game text: ', HraZvolHru.ZvolHru.gameOver(request) ],
                })
                    
                elif response == "go": 
                    x=HraZvolHru.Hry.vyberHru(request)
                    if x is None:
                        HraZvolHru.ZvolHru.losPokojeZacatek(request)
                    HraZvolHru.ZvolHru.jit(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.ZvolHru.vyberPokoje(request) ],       
                })
                    
                elif response == "nova":
                    HraZvolHru.ZvolHru.losPokojeZacatek(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.ZvolHru.vyberPokoje(request) ],       
                })
                
                elif 'newGame' in request.POST:
                    HraZvolHru.ZvolHru.losPokojeZacatek(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                #'text2':['Game text: ', HraZvolHru.ZvolHru.losPokojeZacatek(request) ],       
                'text3':['Push Submit and play again.',''],
                })
                    
                elif response=="":
                    x=HraZvolHru.Hry.vyberHru(request)
                    if x is None:
                        HraZvolHru.ZvolHru.losPokojeZacatek(request)
                    HraZvolHru.ZvolHru.prazdnaOdpoved(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.ZvolHru.vyberPokoje(request) ],       
                'text3':['Game text: ', HraZvolHru.ZvolHru.prazdnaOdpoved(request) ],
                })
                
                if request.session.get('pokoj') == 'livingroom' and response =="guess higher number":
                    HraZvolHru.Hry.HraVyssiBere.uvod(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.Hry.HraVyssiBere.uvod(request) ],       
                #'text3':['Game text: ', HraZvolHru.ZvolHru.prazdnaOdpoved(request) ],
                })
                
                if request.session.get('pokoj') == 'livingroom' and response.isdigit():
                    HraZvolHru.Hry.HraVyssiBere.hraVyssiBere(request, response)
                    HraZvolHru.ZvolHru.gameOver(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.Hry.HraVyssiBere.hraVyssiBere(request, response) ], 
                'text3':['Game text: ', HraZvolHru.ZvolHru.gameOver(request) ],
                })
                
                if request.session.get('pokoj') == 'livingroom' and response =="even or odd":
                    HraZvolHru.Hry.HraSudaLicha.uvod(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.Hry.HraSudaLicha.uvod(request) ],       
                #'text3':['Game text: ', HraZvolHru.ZvolHru.prazdnaOdpoved(request) ],
                })
                
                if request.session.get('pokoj') == 'livingroom' and response =="even" or response == "odd":
                    HraZvolHru.Hry.HraSudaLicha.hraSudaLicha(request, response)
                    HraZvolHru.ZvolHru.gameOver(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.Hry.HraSudaLicha.hraSudaLicha(request, response) ],
                'text3':['Game text: ', HraZvolHru.ZvolHru.gameOver(request) ],
                #'text3':['Game text: ', HraZvolHru.ZvolHru.prazdnaOdpoved(request) ],
                })
                
                if request.session.get('pokoj') == 'bedroom' and response =="hide and seek":
                    HraZvolHru.Hry.HraSchovavana.uvod(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.Hry.HraSchovavana.uvod(request) ],       
                #'text3':['Game text: ', HraZvolHru.ZvolHru.prazdnaOdpoved(request) ],
                })
                
                if request.session.get('pokoj') == 'bedroom' and response =="bed" or response == "wardrobe" or response =="screen":
                    HraZvolHru.Hry.HraSchovavana.hraSchovavana(request, response)
                    HraZvolHru.ZvolHru.gameOver(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.Hry.HraSchovavana.hraSchovavana(request, response) ],
                'text3':['Game text: ', HraZvolHru.ZvolHru.gameOver(request) ],
                #'text3':['Game text: ', HraZvolHru.ZvolHru.prazdnaOdpoved(request) ],
                })
                
                if request.session.get('pokoj') == 'bathroom' and response =="bath":
                    HraZvolHru.Hry.HraKoupel.uvod(request)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.Hry.HraKoupel.uvod(request) ],       
                #'text3':['Game text: ', HraZvolHru.ZvolHru.prazdnaOdpoved(request) ],
                })
                
                if request.session.get('pokoj') == 'bathroom' and response =="giant bath":
                    HraZvolHru.Hry.HraKoupel.hraKoupel(request, response)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', HraZvolHru.Hry.HraKoupel.hraKoupel(request, response) ],       
                #'text3':['Game text: ', HraZvolHru.ZvolHru.prazdnaOdpoved(request) ],
                })
                
                if request.session.get('pokoj') == 'bathroom' and response == str(response):
                    #HraZvolHru.Hry.HraKoupel.hraKoupel(request, response)
                    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text2':['Game text: ', 'You should take a bath finally!' ],       
                #'text3':['Game text: ', HraZvolHru.ZvolHru.prazdnaOdpoved(request) ],
                })
                                        
    return render(request, 'projects/zvolhru.html',{
                'userName': ['User: ',kokiUser(request)],
                'text1':['Text', 'Enter your number.'],
                #'text2':['Game text: ', HraZvolHru.ZvolHru.vyberPokoje(request) ],
                #'text2':['Game text: ', HraZvolHru.Hry.hraVyssiBere(request) ],
                #'text2':['Game text: ', HraZvolHru.ZvolHru.vyberHratJit(request) ],
                'text3':['Push Submit and play.', '' ],
                })
    
def flashBulb(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    def turnOnOff(request):
        if 'turnon' in request.POST:
            onOff = 'disabled'
        else:
            onOff = ''
        return onOff
    
    return render(request, 'projects/flashbulb.html',{
                'userName': ['User: ',kokiUser(request)],
                #'text1':[turnOnOff(request)],
                })
    
def clocks(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    return render(request, 'projects/clocks.html',{
                'userName': ['User: ',kokiUser(request)],
                })

def pexeso(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    return render(request, 'projects/pexeso.html',{
                'userName': ['User: ',kokiUser(request)],
                })

def battlefield(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    return render(request, 'projects/battlefield.html',{
                'userName': ['User: ',kokiUser(request)],
                })

def midnightpaddleball(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    return render(request, 'projects/midnightpaddleball.html',{
                'userName': ['User: ',kokiUser(request)],
                })

def starwar(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    return render(request, 'projects/starWar.html',{
                'userName': ['User: ',kokiUser(request)],
                })

def spa(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    # return render(request, 'poll/index.html',{
    
    # pdb.set_trace()
    return render(request, 'projects/spa.html',{
                'userName': ['User: ',kokiUser(request)],
                })

def calculator2(request):
    def kokiUser(request):
        if 'userName' in request.session:
            userName = request.session.get('userName')
            return userName
        
    return render(request, 'projects/calculator2.html',{
                'userName': ['User: ',kokiUser(request)],
                })
