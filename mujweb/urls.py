from django.urls import include, path
from django.views.generic import TemplateView

from . import views
# from poll import views as viewsPoll
# from poll import urls as urlPoll

urlpatterns = [
    path('', views.index, name='index'),
    path('projects/', views.projects, name='projects'),
    path('links/', views.links, name='links'),
    path('signup/', views.signup, name='signup'),
    path('login_page/', views.loginPage, name='login_page'),
    path('logout/', views.logout, name='logout'),
    path('projects/calculator/', views.calculator, name='calculator'),
    path('projects/guessthenumber/', views.guessthenumber, name='guessthenumber'),
    path('projects/textAnalysis/', views.textAnalysis, name='textAnalysis'),
    path('projects/rockpaper/', views.rockPaper, name='rockPaper'),
    path('projects/zvolhru/', views.zvolHru, name='zvolhru'),
    path('projects/flashbulb/', views.flashBulb, name='flashbulb'),
    path('projects/clocks/', views.clocks, name='clocks'),
    path('projects/pexeso/', views.pexeso, name='pexeso'),
    path('projects/battlefield/', views.battlefield, name='battlefield'),
    path('projects/midnightpaddleball/', views.midnightpaddleball, name='midnightpaddleball'),
    path('projects/starwar/', views.starwar, name='starwar'),
	path('projects/spa/', views.spa, name='spa'),
	path('projects/calculator2/', views.calculator2, name='calculator2'),

    # path('', include('poll.urls')),
    # path('spa/', views.IndexView.as_view(), name='index'),
]
