from django import forms
# from django_select2 import forms as s2forms

from . import models
# from .models import MojeOdkazy1


class NameForm(forms.Form):
    # jmeno = forms.CharField(label='Your name', max_length=100)
    # mail_address = forms.CharField(label='E-mail Adrress', max_length=100)
    first_name = forms.CharField(label='First name', max_length=100)
    last_name = forms.CharField(label='Last name', max_length=100)
    email = forms.CharField(label='E-mail Adrress', max_length=100)





# class KeyWidget(s2forms.ModelSelect2Widget):
#     search_fields = [
#         "username__icontains",
#         "email__icontains",
#     ]


# class ValueWidget(s2forms.ModelSelect2MultipleWidget):
#     search_fields = [
#         "username__icontains",
#         "email__icontains",
#     ]

# class BookForm(forms.ModelForm):
#     class Meta:
#         model = models.MojeOdkazy1
#         fields = "__all__"
#         widgets = {
#             "author":  KeyWidget,
#             "co_authors": ValueWidget,
#             # 'xxx': XxxWidget,
#             # 'myfield': forms.TextInput(attrs={'class': 'myfieldclassXXX'}),
#         }


# class MyForm(forms.ModelForm):
#     class Meta:
#         model = MyModel
#         widgets = {
#             'myfield': forms.TextInput(attrs={'class': 'myfieldclass'}),
#         }