from django.contrib import admin

from .models import Choice, Question, MojeOdkazy1


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3
    max_num = 3

    def has_delete_permission(self, request, obj=None):
          return False


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['question_text','question_type']})
    ]
    inlines = [ChoiceInline]

    list_display = ('question_text', 'pokus', 'question_type')

    search_fields = ['question_text']

    # def has_add_permission(self, request):
    #     return False
    # def has_delete_permission(self, request, obj=None):
    #     return False

class MojeOdkazy1Admin(admin.ModelAdmin):
    # list_display = ('key', 'value')
    a=0

admin.site.register(Question, QuestionAdmin)
admin.site.register(MojeOdkazy1)