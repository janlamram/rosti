from django.db import models

from django.conf import settings
import pdb

# Create your models here.

class Users(models.Model):
    jmeno = models.CharField(max_length = 20)
    mail_address = models.CharField(max_length = 40, default = None)

    def __str__(self):
        return self.jmeno

class Question(models.Model):
    question_text = models.CharField(max_length = 200)
    question_type = models.IntegerField(default=1)
    pokus = 'pokus'

    def __str__(self):
        return self.question_text

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length = 100)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

class MojeOdkazy1(models.Model):
    # pdb.set_trace()
    key = models.CharField(max_length=45, blank=True, null=True)
    value = models.CharField(max_length=200, blank=True, null=True)
    xxx = 'xxxxxxx'

    class Meta:
        managed = False
        db_table = 'moje_odkazy_1'
        verbose_name='Odkazy'
    
    def __str__(self):
        #  return '%s %s' % (self.key, self.value)
        odkaz = str({'key': self.key, 'value': self.value})
        return odkaz
