from django.urls import include, path
# from django.urls import path

from . import views

app_name = 'poll'
urlpatterns = [
    # path('', views.index, name='index'),
    path('', views.IndexView.as_view(), name='index'),
    path('results/', views.results, name='results'),
    path('vote/', views.vote, name='vote'),
    path('uloz/', views.uloz, name='uloz'),
    # path('apiapp/', views.ApiAppView.as_view(), name='apiapp'),
    # path('databaseApp/', views.databaseApp, name='databaseApp'),
    # path('twitterApp/', views.twitterapp, name='twitterapp'),
    path('changeapp/', views.changeApp, name='changeapp'),
    path('changeDlazdice/', views.changeDlazdice, name='changeDlazdice'),
    path('changePagin/', views.changePagin, name='changePagin'),
    path('get_name/', views.get_name, name='get_name'),
    # path('forms/', views.forms, name='forms'),

    # path('changeDlazdice/<int:question_type>', views.changeDlazdice, name='changeDlazdice'),
    # path("select2/", include("django_select2.urls")),
    # path("book/create", views.BookCreateView.as_view(), name="book-create"),
    
    path("modal_window/", views.modal_window, name="modal_window"),
]