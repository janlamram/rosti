from django.shortcuts import  get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest, HttpResponse
from django.urls import reverse
from django.views import generic
import logging
import json
from .models import Question, Choice, MojeOdkazy1, Users
import requests
import time
from django.views.generic import ListView
from django.core.paginator import Paginator
import pdb
from . import forms, models
# from .forms import NameForm, BookForm
import random
import string

from django.core.mail import send_mail
from django.conf import settings

from django.contrib.auth.models import User
#regular expressions
import re
from django.contrib.auth.models import Permission
from django.template import loader
import base64
from .forms import NameForm


def modal_window(request):

    if request.method == 'GET':
        # pdb.set_trace()

        #TODO - Try:
        title = request.GET['title'].upper()
        url = request.GET['url']

        #získáme obrázek, převedeme na base64
        # URL = url
        r = requests.get(url)
        image = r.content
        encoded_string = base64.b64encode(image)
        file = 'data:image/png;base64,'  + str(encoded_string.decode('utf-8'))


        context = {
            'title': title,
            'url': url,
            'image': file
        }

    return render(request,'components/modal_window.html', context)

def get_name(request):
    logger = logging.getLogger("AppPoll")
    logger.info("get name 1")
    regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'

    if request.method == 'POST':
        form =  json.loads(request.POST['formData'])
        try:
            first_name = form['first_name']
            last_name = form['last_name']
            email = form['email']

        except (KeyError, Choice.DoesNotExist):
            return render(request,'poll/forms.html',{'message': 'Not valid inputs. Click Form and try it again. No data available.'})

        else:
            def checkEmail(email):
                if(re.search(regex,email)):
                    #"Valid Email"
                    return True
                else:
                    #"Invalid Email"
                    return False

            #kontrola, zda ve jmenech není číslovka
            def hasNumbers(inputString):
                return bool(re.search(r'\d', inputString))
            first_name_hasNum = hasNumbers(first_name)
            last_name_hasNum = hasNumbers(last_name)

            def inputCheck(inputString):
                if '<' in inputString or '>' in inputString or "'" in inputString or '"' in inputString or ' ' in inputString:
                    return False

            form_isValid = True;
            if (first_name == '' or first_name_hasNum == True or last_name_hasNum == True or last_name == '' or checkEmail(email) == False or inputCheck(first_name) == False or inputCheck(last_name) == False):
                form_isValid = False;


            if form_isValid:
                # vytvoř heslo
                password = ''
                for i in range(5):
                    num = random.randint(0, 9)
                    letter = random.choice(string.ascii_letters)
                    password += str(num)
                    password += letter

                #kontrola, zda není již stejné username v db, username je email
                num_results = User.objects.filter(username = email).count()

                if(num_results < 1 ):
                    logger.info("get name 2")
                    #vytváříme usera, první argument je username, musí být unikátní
                    user = User.objects.create_user(email)
                    # user = User.objects.get(username = 'kamil')
                    logger.info("get name 3")
                    user.first_name = first_name
                    user.last_name = last_name
                    # user.username = form['last_name']
                    logger.info("get name 4")
                    user.email = email
                    #heslo se hashuje
                    logger.info("get name 5")
                    user.set_password(password)
                    # is_staff = 1, user může do admin sekce
                    logger.info("get name 6")
                    user.is_staff = True
                    logger.info("get name 7")
                    
                    # permission = Permission.objects.get(name='Can view Odkazy')
                    
                    #TODO: Can view question je v polls i poll - odlišit
                    logger.info("get name 8")
                    
                    # permission2 = Permission.objects.get(name='Can view question')
                    
                    logger.info("get name 9")
                    # user.user_permissions.add(permission, permission2)
                    logger.info("get name 10")

                    user.save()

                    html_message = loader.render_to_string(
                        'poll/email_template.txt',
                        {
                            'user_name': user.first_name + ' ' + user.last_name,
                            'subject':   password,

                        }
                    ).strip()

                    logger.info("get name 11")

                    #posleme email
                    # send_mail(
                    #     subject = 'Automatically generated e-mail. JS webpages code.',
                    #     # message = '<p>Here is your security code:<h3> ' + password + ' <h3>. Write it into the log-in input.',
                    #     message = html_message,
                    #     from_email = "janslajchrt81@gmail.com",
                    #     recipient_list = [email,],
                    #     auth_user = 'janslajchrt81@gmail.com',
                    #     auth_password = 'Lmrlmrlmr111',
                    #     fail_silently = False,
                    #     )

                    send_mail(
                        subject = 'Automatically generated e-mail. JS webpages code.',
                        # message = '<p>Here is your security code:<h3> ' + password + ' <h3>. Write it into the log-in input.',
                        message = html_message,
                        from_email = "3006@rostiapp.cz",
                        recipient_list = [email,],
                        auth_user = '3006@rostiapp.cz',
                        auth_password = '179fb6ef67e84625ad7bf402a8a8a321',
                        fail_silently = False,
                        )
                    logger.info("get name 12") 
                else:
                     return render(request,'poll/forms.html',{'message': 'You are already registered.'})
                # return HttpResponseRedirect('http://localhost:8000/admin')
                return HttpResponseRedirect('http://localhost:8000/admin/login/?next=/admin/')
                # return render(request,'poll/forms.html',{'message': 'You can log in to administration sites under the password in your e-mail.'})
            else:
                return render(request,'poll/forms.html',{'message': 'Not valid inputs. Click Form and try it again.'})
    else:
        form = NameForm()
        # return render(request,'poll/forms.html',{'message': 'Not valid inputs. Try it again.'})

    # return render(request, 'poll/forms.html', {'form': form})


# class BookCreateView(generic.CreateView):
#     template_name = 'poll/proj/select.html'
#     model = models.MojeOdkazy1
#     # pdb.set_trace()
#     form_class = forms.BookForm
#     success_url = "/"
#     id_dlazdice = None

#     URL = "https://jsonplaceholder.typicode.com/photos/"
#     r = requests.get(url = URL)
#     data = r.json()
#     # pdb.set_trace()
#     context = {'data':data}


logger = logging.getLogger("AppPoll")
logger.info("Whatever to log")

question_type = None

# meni dlazdice z hlavni listy
def changeApp(request):
    logger = logging.getLogger("AppPoll")

    if request.method == 'GET':
        id_dlazdice = request.GET['id_dlazdice']
        logger.info("IDDDDDDDDDDDDDD type in get----- ")
        logger.info("ID DLAZKA get " + id_dlazdice)

        if id_dlazdice == "index1":
            # question_type = request.GET['question_type']
            question_type = 1
            logger = logging.getLogger("AppPoll")
            logger.info("Get konec APIAPPPPP" + 'po po pio 11111')
            context = {}
            logger.info(str(context))
            return render(request, 'poll/apiapp.html', context)
        elif id_dlazdice == "index2":
            # question_type = request.GET['question_type']
            question_type = 1
            logger = logging.getLogger("AppPoll")
            logger.info("Get konec index 2 question type" + str(question_type))
            context = {'list':{'latest_question_list':Question.objects.filter(question_type = question_type).order_by('pk')[:5], 'choice_list': Choice.objects.all(), 'question_type': question_type}}
            logger.info("Get konec " + str(context))
            return render(request, 'poll/poll.html', context)
        elif id_dlazdice == "index3":
            logger.info("Index 3 -- 1 ")
            form = NameForm()
            logger.info("Index 3 -- 2 ")
            context = {'form':form}
            # return render(request, 'poll/forms.html', context)
            return render(request, 'components/login_admin_form.html', context)


#paginator je v database app
def changePagin(request):
    odkazy = MojeOdkazy1.objects.all()
    # logger.info("Pagin 1 -------------- ")
    paginator = Paginator(odkazy, 2) # Show 2 contacts per page.
    # logger.info("Pagin 1 pagggii object list-------------- " + str(paginator.object_list))

    page_number = request.GET.get('pageNum')
    # pdb.set_trace()
    page_obj = paginator.get_page(page_number)
    # return render(request, 'poll/proj/databaseApp.html', {'page_obj': page_obj})
    return render(request, 'components/paginator1.html', {'page_obj': page_obj})
    # return {'page_obj': page_obj}


# meni dlazdice v poll
def changeDlazdice(request):
    logger = logging.getLogger("AppPoll")
    logger.info("Change dlazdice začátek")


    if request.method == 'GET':
        id_dlazdice = request.GET['id_dlazdice']
        #click je string  = 'clicked' / ''

        if(id_dlazdice == 'dlazdiceApiApp1'):
            # pdb.set_trace()

            logger.info("Pagin 1 -------------- pred")
            odkazy = MojeOdkazy1.objects.all()
            logger.info("Pagin 1 -------------- za" + str(odkazy))
            paginator = Paginator(odkazy, 2) # Show 2 contacts per page.
            logger.info("Pagin 1 pagggii object list-------------- " + str(paginator.object_list))
            page_number = request.GET.get('pageNum')
            page_obj = paginator.get_page(page_number)
            logger.info("Pagin 1 page_obj -------------- " + str(page_obj))
            # paginBlock = render(request, 'components/paginator1.html', {'page_obj': page_obj})
            # return render(request, 'poll/proj/databaseApp.html', {'id_dlazdice': id_dlazdice, 'paginBlock': paginBlock.getvalue})
            return render(request, 'poll/proj/databaseApp.html', {'id_dlazdice': id_dlazdice, 'page_obj': page_obj})


        # if(id_dlazdice == 'dlazdiceApiApp1'):
        #     return DatabaseAppView.as_view(id_dlazdice=id_dlazdice)(request)


        if(id_dlazdice == 'dlazdiceApiApp2'):
            logger.info("ApiApp dlazdice 2 -------------- ")
            context = {
                'items':
                [
                    {'value': 'https://twitter.com/BarackObama?ref_src=twsrc%5Etfw', 'title':'Tweets by Barack Obama'},
                    {'value': 'https://twitter.com/kalousekm?ref_src=twsrc%5Etfw', 'title' : 'Tweets by Míra Kalous'},
                    {'value': 'https://twitter.com/rickygervais?ref_src=twsrc%5Etfw', 'title' : 'Tweets by Ricky Gervais'},
                ],
                'id_dlazdice': id_dlazdice
            }
            return render(request, 'poll/proj/twitterApp.html', context)



        if(id_dlazdice == 'dlazdiceApiApp3'):
            logger.info("ApiApp dlazdice 3 -------------- ")

            # URL = "https://www.zakonyprolidi.cz/api/v1/data.json/DocTypeList?apikey=test"
            # r = requests.get(url = URL)
            # data = r.json()

            #TODO: dodělat try

            URL = "https://jsonplaceholder.typicode.com/photos/"
            r = requests.get(url = URL)
            data = r.json()
            context = {'data':data}
            return render(request, 'poll/proj/signpost.html', context)

        # if(id_dlazdice == 'dlazdiceApiApp4'):
        #     # pdb.set_trace()
        #     return BookCreateView.as_view(id_dlazdice=id_dlazdice)(request)
        #     # return BookCreateView.as_view()(request)



        if(id_dlazdice == 'dlazdicePoll'):
            question_type = request.GET['question_type']
            logger.info("Question type in changeDlazdice " + question_type)
            context = {'list':{'latest_question_list':Question.objects.filter(question_type = question_type).order_by('pk')[:5], 'choice_list': Choice.objects.all(), 'question_type': question_type}}
            return render(request, 'poll/poll.html', context)


# def databaseAppContext(id_dlazdice):
#     context = {'odkazy': MojeOdkazy1.objects.all(), 'id_dlazdice': id_dlazdice}
#     # time.sleep(2)
#     return  context


# class ContactList(ListView):
#     paginate_by = 2
#     model = Contact


class DatabaseAppView(generic.ListView):
    template_name = 'poll/proj/databaseApp.html'
    context_object_name = 'list'
    id_dlazdice = None

    paginate_by = 2
    model = MojeOdkazy1

    def get_queryset(self):
        logger.info("Database App view Class get Query Set --- " + str(MojeOdkazy1.objects.all()))
        return{'odkazy': MojeOdkazy1.objects.all(), 'id_dlazdice':self.id_dlazdice}


# if(id_dlazdice == 'dlazdiceApiApp1'):
        #     odkazy = MojeOdkazy1.objects.all()
        #     # logger.info("Pagin 1 -------------- ")
        #     paginator = Paginator(odkazy, 2) # Show 2 contacts per page.
        #     # logger.info("Pagin 1 pagggii object list-------------- " + str(paginator.object_list))
        #     page_number = request.GET.get('pageNum')
        #     page_obj = paginator.get_page(page_number)
        #     # logger.info('Page object' +  str(page_obj) +str(page_obj.__class__) +str(page_obj[0]) + str(page_obj[1]))
        #     return render(request, 'poll/proj/databaseApp.html', {'page_obj': page_obj})


class IndexView(generic.ListView):
    template_name = 'poll/index.html'
    # template_name = 'poll/apiapp.html' 
    # template_name = 'poll/proj/databaseApp.html'
    # template_name = 'poll/proj/databaseApp.html'
    # context_object_name = 'list'
    # question_type = 1

    logger = logging.getLogger("AppPoll")
    logger.info("Poll/views --------")


    def get_queryset(self):
        # self.changeDlazdice()
        return {}
        # return {'latest_question_list':Question.objects.filter(question_type = self.question_type).order_by('pk')[:5], 'choice_list': Choice.objects.all(), 'question_list' : Question.objects.all(), 'question_id': Question.objects.get(pk=self.question_type).id,'question_type':self.question_type}



# class ApiAppView(generic.ListView):
#     template_name = "poll/apiapp.html"
#     context_object_name = 'list'

#     logger = logging.getLogger("AppPoll")
#     logger.info("TTTuuuuuuuuuuuuuuuuuu ")

    # def get_queryset(self):
    #     return {'latest_question_list':Question.objects.filter(question_type = self.question_type).order_by('pk')[:5], 'choice_list': Choice.objects.all(), 'question_list' : Question.objects.all(), 'question_id': Question.objects.get(pk=self.question_type).id,'question_type':self.question_type}




# def index(request):
#     # latest_question_list = Question.objects.order_by('-pub_date')[:5]
#     latest_question_list = Question.objects.order_by('pk')[:5]
#     choice_list = Choice.objects.all()
#     # choice_list = Choice.objects.order_by('pk')
#     # choice_list = Choice.objects.filter(question_id=2)
#     context = {'latest_question_list': latest_question_list, 'choice_list': choice_list}
#     return render(request, 'poll/index.html', context)


def results(request):
    latest_question_list = Question.objects.order_by('pk')[:5]
    choice_list = Choice.objects.all()
    context = {'latest_question_list': latest_question_list, 'choice_list': choice_list}
    return render(request, 'poll/results.html', context)


# def vote(request):
#     # question = get_object_or_404(Question, pk=question_id)
#     #  question = Question.objects.all()

#     latest_question_list = Question.objects.filter(question_type=1).order_by('pk')[:5]
#     for question in latest_question_list:
#         # question = get_object_or_404(Question, question.id)
#         selected_choice = question.choice_set.get(pk=request.POST['choiceForQuestion_' + str(question.id)])
#         selected_choice.votes += 1
#         selected_choice.save()
#     # return HttpResponse(reverse('poll:index'))
#     return HttpResponseRedirect(reverse('poll:index'))
#     # return HttpResponseRedirect(reverse('poll:results'))


def vote(request):
    # question = get_object_or_404(Question, pk=question_id)
    #  question = Question.objects.all()
    question_type = request.POST.get('question_type')

    latest_question_list = Question.objects.filter(question_type=question_type).order_by('pk')[:5]
    logger = logging.getLogger("AppPoll")
    logger.info("latest_question_list: " + str(latest_question_list))

    for question in latest_question_list:
        logger = logging.getLogger("AppPoll")
        logger.info("For loop: " + str(question))

        # question = get_object_or_404(Question, question.id)
        try:
            selected_choice = question.choice_set.get(pk=request.POST['choiceForQuestion_' + str(question.id)])
        except (KeyError, Choice.DoesNotExist):
            # return HttpResponseRedirect(reverse('poll:index'))
            a=0 #pouze, aby tu něco bylo, jinak není potřeba
        else:
            logger = logging.getLogger("AppPoll")
            logger.info("Selected choice: ")
            logger.info("Selected choice s ID: " + str(selected_choice.id) + " Text: " + str(selected_choice))
            # logger.info("Choice_set_get For loop: " + str(question.choice_set.get(pk=request.POST['choiceForQuestion_1'])))

            selected_choice.votes += 1
            selected_choice.save()

        logger.info("Choice_set_all() After For loop: " + str(question.choice_set.all()))

    return HttpResponseRedirect(reverse('poll:index'))


def uloz(request):
    logger = logging.getLogger("AppPoll")
    question_type = request.POST.get('question_type')

    if request.method == 'POST':
        logger = logging.getLogger("AppPoll")
        logger.info("Ulozzzz uvnitr  pred------ " + request.POST['formData'])

        #vytváří ze stringu json
        _data = json.loads(request.POST['formData'])

        for choice in _data:
            #_data[choice] je id zakliknutého radio buttonu
            _votes = Choice.objects.get(pk=_data[choice]).votes + 1
            #updatni v db
            Choice.objects.filter(id=_data[choice]).update(
                votes = _votes
            )
    context = {'list':{'latest_question_list':Question.objects.filter(question_type = question_type).order_by('pk')[:5], 'choice_list': Choice.objects.all(), 'question_list' : Question.objects.all(),'question_type':  question_type}}
    return render(request, 'poll/poll.html', context)



# def vote (request):
    # return HttpResponseRedirect('https://www.seznam.cz/')
    # return HttpResponse("hkhkkjg Redir")
    # return HttpResponse(Question.objects.order_by('pk')[:5])
    # latest_question_list = Question.objects.order_by('-pub_date')[:5]
    # latest_question_list = Question.objects.order_by('pk')[:5]
    # choice_list = Choice.objects.all()

    # for question_id in latest_question_list:
    #     question = get_object_or_404(Question, pk=question_id)
    #     selected_choice = question.choice_set.get(pk=request.POST['choice'])
    #     selected_choice.votes += 1
    #     selected_choice.save()

    # choice_list = Choice.objects.order_by('pk')
    # choice_list = Choice.objects.filter(question_id=2)
    # context = {'latest_question_list': latest_question_list, 'choice_list': choice_list}
    # return render(request, 'poll/results.html', context)
    # return HttpResponseRedirect('poll:results')

# def vote(request, question_id):

#     question = get_object_or_404(Question, question_id)
#     try:
#         selected_choice = question.choice_set.get(pk=request.POST['choice'])
#     except (KeyError, Choice.DoesNotExist):
#         # Redisplay the question voting form.
#         return render(request, 'poll/index.html', {
#             'question': question,
#             'error_message': "You didn't select a choice.",
#         })
#     else:
#         selected_choice.votes += 1
#         selected_choice.save()
#         # Always return an HttpResponseRedirect after successfully dealing
#         # with POST data. This prevents data from being posted twice if a
#         # user hits the Back button.
#         # return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
#         return HttpResponseRedirect(reverse('poll:index', args=(question.id,)))


# def index(request):
#     return render(request, 'poll/index.html')

# return HttpResponse("Hello, world. You're at the polls index.")